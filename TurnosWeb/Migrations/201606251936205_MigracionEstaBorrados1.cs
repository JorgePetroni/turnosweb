namespace TurnosWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MigracionEstaBorrados1 : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Paciente", new[] { "ApplicationUser_Id" });
            DropColumn("dbo.Paciente", "UsuarioId");
            RenameColumn(table: "dbo.Paciente", name: "ApplicationUser_Id", newName: "UsuarioId");
            AlterColumn("dbo.Paciente", "UsuarioId", c => c.String(maxLength: 128));
            CreateIndex("dbo.Paciente", "UsuarioId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Paciente", new[] { "UsuarioId" });
            AlterColumn("dbo.Paciente", "UsuarioId", c => c.Guid(nullable: false));
            RenameColumn(table: "dbo.Paciente", name: "UsuarioId", newName: "ApplicationUser_Id");
            AddColumn("dbo.Paciente", "UsuarioId", c => c.Guid(nullable: false));
            CreateIndex("dbo.Paciente", "ApplicationUser_Id");
        }
    }
}
