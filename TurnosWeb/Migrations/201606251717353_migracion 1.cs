namespace TurnosWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class migracion1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Especialidad",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Descripcion = c.String(nullable: false, maxLength: 80),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Medico",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        EspecialidadId = c.Guid(nullable: false),
                        Nombre = c.String(nullable: false, maxLength: 80),
                        Apellido = c.String(nullable: false, maxLength: 80),
                        Email = c.String(maxLength: 128),
                        Direccion = c.String(),
                        Telefono = c.String(),
                        Matricula = c.String(nullable: false, maxLength: 8),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Especialidad", t => t.EspecialidadId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.Email)
                .Index(t => t.EspecialidadId)
                .Index(t => t.Email);
            
            CreateTable(
                "dbo.Horario",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        MedicoId = c.Guid(nullable: false),
                        HoraInicio = c.Time(nullable: false, precision: 7),
                        HoraFin = c.Time(nullable: false, precision: 7),
                        Monday = c.Boolean(nullable: false),
                        Tuesday = c.Boolean(nullable: false),
                        Wednesday = c.Boolean(nullable: false),
                        Thursday = c.Boolean(nullable: false),
                        Friday = c.Boolean(nullable: false),
                        Saturday = c.Boolean(nullable: false),
                        Sunday = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Medico", t => t.MedicoId, cascadeDelete: true)
                .Index(t => t.MedicoId);
            
            CreateTable(
                "dbo.Turno",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        MedicoId = c.Guid(nullable: false),
                        PacienteId = c.Guid(nullable: false),
                        Fecha = c.DateTime(nullable: false),
                        HoraTurno = c.Time(nullable: false, precision: 7),
                        EstadoTurno = c.Int(nullable: false),
                        ObraSocial_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Medico", t => t.MedicoId, cascadeDelete: true)
                .ForeignKey("dbo.ObraSocial", t => t.ObraSocial_Id)
                .ForeignKey("dbo.Paciente", t => t.PacienteId, cascadeDelete: true)
                .Index(t => t.MedicoId)
                .Index(t => t.PacienteId)
                .Index(t => t.ObraSocial_Id);
            
            CreateTable(
                "dbo.Paciente",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        UsuarioId = c.Guid(nullable: false),
                        Nombre = c.String(nullable: false, maxLength: 250),
                        Apellido = c.String(nullable: false, maxLength: 250),
                        Telefono = c.String(maxLength: 25),
                        Celular = c.String(maxLength: 250),
                        Direccion = c.String(maxLength: 400),
                        Dni = c.String(nullable: false, maxLength: 8),
                        Email = c.String(maxLength: 250),
                        FechaNacimiento = c.DateTime(nullable: false),
                        EstaBorrado = c.Boolean(nullable: false),
                        EstadoCivil = c.Int(nullable: false),
                        GrupoSanguineo = c.Int(nullable: false),
                        Sexo = c.Int(nullable: false),
                        ApplicationUser_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUser_Id)
                .Index(t => t.ApplicationUser_Id);
            
            CreateTable(
                "dbo.ObraSocial",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Descripcion = c.String(nullable: false),
                        Codigo = c.String(nullable: false),
                        Abreviacion = c.String(nullable: false, maxLength: 5),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.ObraSocialPacientes",
                c => new
                    {
                        ObraSocial_Id = c.Guid(nullable: false),
                        Paciente_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.ObraSocial_Id, t.Paciente_Id })
                .ForeignKey("dbo.ObraSocial", t => t.ObraSocial_Id, cascadeDelete: true)
                .ForeignKey("dbo.Paciente", t => t.Paciente_Id, cascadeDelete: true)
                .Index(t => t.ObraSocial_Id)
                .Index(t => t.Paciente_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.Medico", "Email", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Paciente", "ApplicationUser_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Turno", "PacienteId", "dbo.Paciente");
            DropForeignKey("dbo.Turno", "ObraSocial_Id", "dbo.ObraSocial");
            DropForeignKey("dbo.ObraSocialPacientes", "Paciente_Id", "dbo.Paciente");
            DropForeignKey("dbo.ObraSocialPacientes", "ObraSocial_Id", "dbo.ObraSocial");
            DropForeignKey("dbo.Turno", "MedicoId", "dbo.Medico");
            DropForeignKey("dbo.Horario", "MedicoId", "dbo.Medico");
            DropForeignKey("dbo.Medico", "EspecialidadId", "dbo.Especialidad");
            DropIndex("dbo.ObraSocialPacientes", new[] { "Paciente_Id" });
            DropIndex("dbo.ObraSocialPacientes", new[] { "ObraSocial_Id" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.Paciente", new[] { "ApplicationUser_Id" });
            DropIndex("dbo.Turno", new[] { "ObraSocial_Id" });
            DropIndex("dbo.Turno", new[] { "PacienteId" });
            DropIndex("dbo.Turno", new[] { "MedicoId" });
            DropIndex("dbo.Horario", new[] { "MedicoId" });
            DropIndex("dbo.Medico", new[] { "Email" });
            DropIndex("dbo.Medico", new[] { "EspecialidadId" });
            DropTable("dbo.ObraSocialPacientes");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.ObraSocial");
            DropTable("dbo.Paciente");
            DropTable("dbo.Turno");
            DropTable("dbo.Horario");
            DropTable("dbo.Medico");
            DropTable("dbo.Especialidad");
        }
    }
}
