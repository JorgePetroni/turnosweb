namespace TurnosWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class correccioMedicoUsuario : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Medico", "Email", "dbo.AspNetUsers");
            DropIndex("dbo.Medico", new[] { "Email" });
            AddColumn("dbo.Medico", "UsuarioId", c => c.String(maxLength: 128));
            AlterColumn("dbo.Medico", "Email", c => c.String());
            CreateIndex("dbo.Medico", "UsuarioId");
            AddForeignKey("dbo.Medico", "UsuarioId", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Medico", "UsuarioId", "dbo.AspNetUsers");
            DropIndex("dbo.Medico", new[] { "UsuarioId" });
            AlterColumn("dbo.Medico", "Email", c => c.String(maxLength: 128));
            DropColumn("dbo.Medico", "UsuarioId");
            CreateIndex("dbo.Medico", "Email");
            AddForeignKey("dbo.Medico", "Email", "dbo.AspNetUsers", "Id");
        }
    }
}
