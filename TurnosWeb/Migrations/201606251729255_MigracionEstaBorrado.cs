namespace TurnosWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MigracionEstaBorrado : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Especialidad", "EstaBorrado", c => c.Boolean(nullable: false));
            AddColumn("dbo.Medico", "EstaBorrado", c => c.Boolean(nullable: false));
            AddColumn("dbo.Turno", "EstaBorrado", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Turno", "EstaBorrado");
            DropColumn("dbo.Medico", "EstaBorrado");
            DropColumn("dbo.Especialidad", "EstaBorrado");
        }
    }
}
