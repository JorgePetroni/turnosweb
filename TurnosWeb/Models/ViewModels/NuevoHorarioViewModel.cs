﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TurnosWeb.Models.ViewModels
{
    public class NuevoHorarioViewModel
    {
        [ScaffoldColumn(false)]
        public Guid? Id { get; set; }
        [ScaffoldColumn(false)]
        public Guid MedicoId { get; set; }

        [Display(Name = "Duracion del Turno")]
        [Required(ErrorMessage ="Campo Obligatorio")]
        public TimeSpan DuracionTurno { get; set; }
        [Display(Name = "Hora Entrada")]
        [Required(ErrorMessage = "Campo Obligatorio")]
        public TimeSpan HoraInicio { get; set; }
        [Display(Name = "Hora Salida")]
        [Required(ErrorMessage = "Campo Obligatorio")]
        public TimeSpan HoraFin { get; set; }
        [Display(Name = "Lunes")]
        public bool Monday { get; set; }
        [Display(Name = "Martes")]
        public bool Tuesday { get; set; }
        [Display(Name = "Miercoles")]
        public bool Wednesday { get; set; }
        [Display(Name = "Jueves")]
        public bool Thursday { get; set; }
        [Display(Name = "Viernes")]
        public bool Friday { get; set; }
        [Display(Name = "Sabado")]
        public bool Saturday { get; set; }
        [Display(Name = "Domingo")]
        public bool Sunday { get; set; }

        [ScaffoldColumn(false)]
        public DateTime FechaCreacion { get; set; }
    }
}
