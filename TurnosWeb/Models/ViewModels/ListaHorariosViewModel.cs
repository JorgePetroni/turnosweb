﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TurnosWeb.Models.ViewModels
{
    public class ListaHorariosViewModel
    {
        [ScaffoldColumn(false)]
        public Guid HorarioId { get; set; }
        [Display(Name ="Día")]
        public string Dia { get; set; }
        [Display(Name ="Hora Entrada")]
        public TimeSpan HoraInicio { get; set; }
        [Display(Name = "Hora Salida")]
        public TimeSpan HoraFin { get; set; }
        [Display(Name = "Duracion del Turno")]
        public TimeSpan DuracionTurno { get; set; }
        [Display(Name = "Cantidad de Turnos")]
        public int CantidadTurnos { get; set; }
    
    }
}
