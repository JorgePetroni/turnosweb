﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TurnosWeb.Models.ViewModels
{
   public class TurnoVM
    {
        [ScaffoldColumn(false)]
        public Guid? TurnoId { get; set; }
        [ScaffoldColumn(false)]
        public Guid MedicoId { get; set; }
        [ScaffoldColumn(false)]
        public Guid? PacienteId { get; set; }

        
        public string Medico { get; set; }
       
        [Display(Name ="Fecha")]
        public DateTime Fecha { get; set; }
        [Display(Name = "Numero de Turno")]
        public int NumeroTurno { get; set; }
        [Display(Name = "Hora del Turno")]
        public TimeSpan HoraTurno { get; set; }
        [Display(Name = "Estado del Turno")]
        public string EstadoTurno { get; set; }
        [ScaffoldColumn(false)]
        public bool EstaBorrado { get; set; }
        
    }
}
