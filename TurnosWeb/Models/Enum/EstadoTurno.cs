﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TurnosWeb.Models.Enum
{
    public enum EstadoTurno
    {

        Cancelado = 1,
        
        Atendido = 2,
        [Description("Reservado")]
        Reservado = 3,
        Confirmado = 4,
        Disponible=5,
        
            
    }
}
