﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TurnosWeb.Models.MetaData;

namespace TurnosWeb.Models
{
    [Table("Medico")]
    [MetadataType(typeof(IMedico))]
    public class Medico : EntidadBase, IMedico
    {
        public Guid EspecialidadId { get; set; }
        public string UsuarioId { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Email { get; set; }
        public string Direccion { get; set; }
        public string Telefono { get; set; }
        public string Matricula { get; set; }

        public bool EstaBorrado { get; set; }

        [NotMapped]
        public string ApyNom { get { return string.Format("{0}, {1}", Apellido, Nombre); } }
        [ForeignKey("EspecialidadId")]
        public virtual Especialidad Especialidad { get; set; }
        [ForeignKey("UsuarioId")]
        public virtual ApplicationUser Usuario { get; set; }
        public virtual ICollection<Horario> Horarios { get; set; }
        public virtual ICollection<Turno> Turnos { get; set; }
    }
}
