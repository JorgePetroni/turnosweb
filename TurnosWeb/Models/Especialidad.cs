﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TurnosWeb.Models.MetaData;

namespace TurnosWeb.Models
{
    [Table("Especialidad")]
    [MetadataType(typeof(IEspecialidad))]
    public class Especialidad:EntidadBase,IEspecialidad
    {
        public string Descripcion { get; set; }
        public bool EstaBorrado { get; set; }
        public ICollection<Medico> Medicos { get; set; } 

    }
}