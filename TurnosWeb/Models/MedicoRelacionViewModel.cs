﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TurnosWeb.Models
{
    public class TurnoViewModel
    {
        public string Email { get; set; }
        public TimeSpan HoraTurno { get; set; }
        public DateTime Fecha { get; set; }
    }
}