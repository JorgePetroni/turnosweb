﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TurnosWeb.Models.MetaData
{
    public interface IMedico
    {
        [Required(AllowEmptyStrings =false,ErrorMessage ="El {0} es un campo obligatorio")]
        [Display(Name ="Nombre")]
        [StringLength(80,ErrorMessage ="el {0} debe ser menor a {1} caracteres")]
        string Nombre { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "El {0} es un campo obligatorio")]
        [Display(Name = "Apellido")]
        [StringLength(80, ErrorMessage = "el {0} debe ser menor a {1} caracteres")]
        string Apellido { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "la {0} es un campo obligatorio")]
        [Display(Name = "Matricula Profesional")]
        [StringLength(8, ErrorMessage = "el {0} debe ser menor a {1} caracteres")]
        string Matricula { get; set; }

        [Display(Name = "Esta Borrado")]
        bool EstaBorrado { get; set; }

    }
}
