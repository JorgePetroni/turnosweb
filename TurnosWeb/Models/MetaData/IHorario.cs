﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TurnosWeb.Models.Enum;

namespace TurnosWeb.Models.MetaData
{
    public interface IHorario
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "El {0} es un Campo Obligatorio")]
        [DataType(DataType.Time)]
        [Display(Name ="Duracion del Turno")]
        TimeSpan DuracionTurno { get; set; }
        [DataType(DataType.Time)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El {0} es un Campo Obligatorio")]
        [Display(Name = "Hora de Inicio")]
        TimeSpan HoraInicio { get; set; }
        [Display(Name = "Hora de Finalizacion")]
        [DataType(DataType.Time)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "El {0} es un Campo Obligatorio")]
        TimeSpan HoraFin { get; set; }

      

        [ScaffoldColumn(false)]
        bool EstaBorrado { get; set; }
        [ScaffoldColumn(false)]
        DateTime FechaCreacion { get; set; }

        [EnumDataType(typeof(DayOfWeek))]
        DayOfWeek DiaDeLaSemana { get; set; }


    }
}
