﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TurnosWeb.Models.Enum;

namespace TurnosWeb.Models.MetaData
{
    public interface ITurno
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "La {0} es obligatoria")]
        [DataType(DataType.Date)]
        DateTime Fecha { get; set; }
        [Required(AllowEmptyStrings =false,ErrorMessage ="La {0} es obligatoria")]
        [DataType(DataType.Time)]
        TimeSpan HoraTurno { get; set; }
     

        [EnumDataType(typeof(EstadoTurno))]
        EstadoTurno EstadoTurno { get; set; }
    }
}
