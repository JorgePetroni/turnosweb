﻿using System;
using System.Collections;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TurnosWeb.Models.Enum;
using TurnosWeb.Models.MetaData;

namespace TurnosWeb.Models
{
    [Table("Horario")]
    [MetadataType(typeof(IHorario))]
    public class Horario:EntidadBase,IHorario
    {

        public Guid MedicoId { get; set; }

        public TimeSpan DuracionTurno { get; set; }
        public TimeSpan HoraInicio { get; set; }
        public TimeSpan HoraFin { get; set; }
 

        public DateTime FechaCreacion { get; set; }

        public DayOfWeek DiaDeLaSemana { get; set; }
        public bool EstaBorrado { get; set; }
        //Propiedades de Navegacion
        [ForeignKey("MedicoId")]
        public virtual Medico Medico { get; set; }

     
    }
}