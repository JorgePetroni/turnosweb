﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TurnosWeb.Models;
using TurnosWeb.Models.Contexto;
using TurnosWeb.Servicios.ViewModels;

namespace TurnosWeb.Servicios
{
    public class PacienteServicio
    {
        public void Insertar(Paciente persona)
        {
            try
            {

                using (var _db=new ApplicationDbContext())
                {
                    var paciente = new Models.Paciente()
                    {
                       
                        Apellido = persona.Apellido,
                        Nombre = persona.Nombre,
                        Celular = persona.Celular,
                        Email = persona.Email,
                        Sexo = persona.Sexo,
                        Telefono = persona.Telefono,
                        GrupoSanguineo = persona.GrupoSanguineo,
                        Dni = persona.Dni,
                        FechaNacimiento = persona.FechaNacimiento,
                        EstaBorrado = false,
                        EstadoCivil = persona.EstadoCivil,
                        Direccion = persona.Direccion
                    };
                    _db.Entry(paciente).State=System.Data.Entity.EntityState.Added;
                }
            }
            catch (Exception ex)
            {
                
            }
        }
        public List<Paciente> ObtenerPacientes()
        {
            try
            {
                using (var _db=new ApplicationDbContext())
                {
                    return _db.Pacientes.Select(x => new Paciente()
                    {
                        Apellido=x.Apellido,
                        Nombre=x.Nombre,
                       // Usuarioid=x.UsuarioId,
                        Celular=x.Celular,
                        Direccion=x.Direccion,
                        Dni=x.Dni,
                        EstadoCivil=x.EstadoCivil,
                        FechaNacimiento=x.FechaNacimiento,
                        GrupoSanguineo=x.GrupoSanguineo,
                        Id=x.Id,
                        Email=x.Email,
                        Sexo=x.Sexo,
                        Telefono=x.Telefono
                         

                    }).ToList();
                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }
            
    }
}
