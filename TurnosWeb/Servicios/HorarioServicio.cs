﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TurnosWeb.Models;
using TurnosWeb.Models.ViewModels;

namespace TurnosWeb.Servicios
{
    public class HorarioServicio
    {
        public void EliminarHorario(ListaHorariosViewModel horarioVM)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    var horarioEliminar = db.Horarios.Find(horarioVM.HorarioId);
                    horarioEliminar.EstaBorrado = true;
                    db.Entry(horarioEliminar).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {

            }
        }

        public List<ListaHorariosViewModel> ObtenerHorariosMedico(Guid medicoId)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    return db.Horarios.Where(x => !x.EstaBorrado)
                                      .Select(x => new ListaHorariosViewModel()
                                      {
                                          Dia = Enum.GetName(typeof(DayOfWeek), x.DiaDeLaSemana),
                                          DuracionTurno = x.DuracionTurno,
                                          HoraFin = x.HoraFin,
                                          HoraInicio = x.HoraInicio,
                                          HorarioId = x.Id,
                                          CantidadTurnos = Convert.ToInt32((x.HoraFin.TotalMinutes - x.HoraInicio.TotalMinutes) / x.DuracionTurno.TotalMinutes)
                                      }).ToList();
                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }

        public void NuevoHorario(NuevoHorarioViewModel horarioVM)
        {
            using (var db = new ApplicationDbContext())
            {

                for (int i = 0; i < 7; i++)
                {
                    var dia = Enum.GetName(typeof(DayOfWeek), i);
                    if ((bool)horarioVM.GetType().GetProperty(dia).GetValue(horarioVM))
                    {
                        DayOfWeek diaEnum;
                        if (Enum.TryParse(dia, out diaEnum))
                        {
                            var update = db.Horarios.Where(x => x.DiaDeLaSemana.HasFlag(diaEnum) && !x.EstaBorrado).FirstOrDefault();
                            if (update != null)
                            {
                                update.EstaBorrado = true;
                                db.Entry(update).State = System.Data.Entity.EntityState.Modified;
                            }
                            db.Horarios.Add(new Horario()
                            {
                                DuracionTurno = horarioVM.DuracionTurno,
                                DiaDeLaSemana = diaEnum,
                                HoraInicio = horarioVM.HoraInicio,
                                HoraFin = horarioVM.HoraFin,
                                FechaCreacion = DateTime.Now,
                                MedicoId = horarioVM.MedicoId,
                                EstaBorrado = false
                            });

                        }
                    }
                }
                db.SaveChanges();
            }
        }


    }
}
