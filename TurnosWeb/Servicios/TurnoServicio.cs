﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TurnosWeb.Models;
using TurnosWeb.Models.Enum;
using TurnosWeb.Models.ViewModels;

namespace TurnosWeb.Servicios
{
    public class TurnoServicio
    {
        public void AgregarTurno(TurnoVM turno)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    if (turno.TurnoId != null)
                    {
                        var _turnoUpdate = db.Turnos.Find(turno.TurnoId);
                        _turnoUpdate.EstadoTurno = EstadoTurno.Reservado;
                        db.Entry(_turnoUpdate).State = System.Data.Entity.EntityState.Modified;
                    }
                    else
                    {
                        var _nuevoTurno = new Turno()
                        {
                            EstaBorrado = false,
                            EstadoTurno=EstadoTurno.Reservado,
                            Fecha=turno.Fecha,
                            HoraTurno=turno.HoraTurno,
                            MedicoId=turno.MedicoId,
                            NumeroTurno=turno.NumeroTurno,
                            PacienteId=(Guid)turno.PacienteId
                        };
                    }
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {

                throw;
            }

        }

        public List<TurnoVM> ObtenerTurnos(Guid medico, DateTime dia)
        {
            using (var db = new ApplicationDbContext())
            {

                HorarioServicio _hServicio = new HorarioServicio();
                List<TurnoVM> listaTurno = new List<TurnoVM>();

                var horario = db.Horarios.Where(x => x.DiaDeLaSemana.HasFlag(dia.DayOfWeek) && !x.EstaBorrado && x.MedicoId == medico).FirstOrDefault();
                var cantidadTurnos = Convert.ToInt32((horario.HoraFin.TotalMinutes - horario.HoraInicio.TotalMinutes) / horario.DuracionTurno.TotalMinutes);
                var horaTurno = horario.HoraInicio;
                for (int i = 0; i < cantidadTurnos; i++)
                {
                    var turnoEnBase = db.Turnos.Where(x => !x.EstaBorrado && x.Fecha == dia && x.HoraTurno == horaTurno && x.MedicoId == medico && x.NumeroTurno == i + 1).FirstOrDefault();
                    if (turnoEnBase != null)
                    {
                        listaTurno.Add(new TurnoVM()
                        {
                            TurnoId = turnoEnBase.Id,
                            EstaBorrado = turnoEnBase.EstaBorrado,
                            EstadoTurno = Enum.GetName(typeof(EstadoTurno), turnoEnBase.EstadoTurno),
                            Fecha = turnoEnBase.Fecha,
                            Medico = turnoEnBase.Medico.ApyNom,
                            HoraTurno = turnoEnBase.HoraTurno,
                            MedicoId = turnoEnBase.MedicoId,
                            NumeroTurno = turnoEnBase.NumeroTurno,
                            PacienteId = turnoEnBase.PacienteId
                        });
                    }
                    else
                    {
                        listaTurno.Add(new TurnoVM()
                        {
                            EstadoTurno = Enum.GetName(typeof(EstadoTurno), EstadoTurno.Disponible),
                            Fecha = dia,
                            Medico = db.Medicos.Find(medico).ApyNom,
                            HoraTurno = horaTurno,
                            MedicoId = medico,
                            NumeroTurno = i + 1,
                            EstaBorrado = false
                        });
                    }

                }

                return listaTurno.OrderBy(x => x.NumeroTurno).ToList();

            }
        }
        public List<Turno> ObtenerTurnosRolMedico(Guid medicoId, DateTime dia)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Turnos.Where(w => !w.EstaBorrado && w.Fecha >= dia).ToList();

            }
        }

        public void CambiarEstadoTurno(TurnoVM turno)
        {
            using (var db = new ApplicationDbContext())
            {
                EstadoTurno _estado;
                var _turnoUpdate = db.Turnos.Find(turno.TurnoId);
                if (Enum.TryParse(turno.EstadoTurno,out _estado))
                {

                    _turnoUpdate.EstadoTurno = _estado;
                }
                db.Entry(turno).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
        }


    }
}
