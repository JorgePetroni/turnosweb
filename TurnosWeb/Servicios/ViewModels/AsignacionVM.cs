﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TurnosWeb.Servicios.ViewModels
{
    public class AsignacionVM
    {
        public Guid Rol { get; set; }
        public Guid Persona { get; set; }
    }
}
