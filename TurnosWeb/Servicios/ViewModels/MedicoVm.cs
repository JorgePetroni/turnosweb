﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TurnosWeb.Servicios.ViewModels
{
    public class MedicoVm
    {
        [ScaffoldColumn(false)]
        public Guid? Id { get; set; }
        [ScaffoldColumn(false)]
        public Guid EspecialidadId { get; set; }


        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Direccion { get; set; }
        public string Telefono { get; set; }
        public string Matricula { get; set; }
    }
}
