﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TurnosWeb.Models;

namespace TurnosWeb.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        ApplicationDbContext db = new ApplicationDbContext();
        ApplicationUserManager us;

        // GET: Admin
        public ActionResult Index()
        {

            us = new ApplicationUserManager(new UserStore<ApplicationUser>(db));
            ViewBag.Personas = ListaPacientes();
            return View();
        }

        [HttpPost]
        public ActionResult Index(string Persona)
        {
            us = new ApplicationUserManager(new UserStore<ApplicationUser>(db));
            var id = db.Users.Where(w => w.Email == Persona).FirstOrDefault().Id;

            us.AddToRole(id, "Medico");
            var deletelist = new string[1];
            deletelist[0] = "Paciente";
            us.RemoveFromRoles(id, deletelist);

            ViewBag.Personas = ListaPacientes();
            return View();
        }

        public ActionResult TurnosPacientes()
        {

            return View(db.Turnos.ToList());
        }

        public IEnumerable<IdentityUser> ListaPacientes()
        {
            var idRol = db.Roles.Where(x => x.Name == "Paciente").FirstOrDefault().Id;
            return db.Users
                    .Where(x => x.Roles.Select(y => y.RoleId).Contains(idRol))
                    .ToList();
        }
    }
}