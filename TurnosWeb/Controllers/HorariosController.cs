﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TurnosWeb.Models;
using TurnosWeb.Servicios;

namespace TurnosWeb.Controllers
{
    public class HorariosController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private HorarioServicio horarioServicio = new HorarioServicio();
        // GET: Horarios
        public ActionResult Index()
        {
            var horarios = db.Horarios.Include(h => h.Medico);
            var usuarioId = User.Identity.GetUserId();
            var medicoId = db.Medicos.FirstOrDefault(x => x.UsuarioId == usuarioId).Id;
            
            
            return View(horarioServicio.ObtenerHorariosMedico(medicoId));
        }

        // GET: Horarios/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Horario horario = db.Horarios.Find(id);
            if (horario == null)
            {
                return HttpNotFound();
            }
            return View(horario);
        }
        
        // GET: Horarios/Create
        public ActionResult Create()
        {
            var id = User.Identity.GetUserId();
            ViewData["Medico"] = db.Medicos.FirstOrDefault(x => x.UsuarioId == id).ApyNom;

            return View();
        }

        // POST: Horarios/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,MedicoId,DuracionTurno,HoraInicio,HoraFin,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday,EstaBorrado,FechaCreacion")] Horario horario)
        {
            var id = User.Identity.GetUserId();
            ViewData["Medico"] = db.Medicos.FirstOrDefault(x => x.UsuarioId == id).ApyNom;

            horario.Id = Guid.NewGuid();
            horario.FechaCreacion = DateTime.Now;
            horario.EstaBorrado = false;
            horario.MedicoId = db.Medicos.FirstOrDefault(x => x.UsuarioId == id).Id;

            object[] horarioValido = ValidarHorarios(horario);

            if (!(bool)horarioValido[0])
            {
                ModelState.AddModelError("Error", (string)horarioValido[1]);
                return View("Create",horario);
            }

            db.Horarios.Add(horario);
            db.SaveChanges();
            return RedirectToAction("Index");

            return View(horario);
        }

        private object[] ValidarHorarios(Horario horario)
        {
            string msj = string.Empty;

            object[] respuesta = new object[2];

            if (horario.HoraInicio > horario.HoraFin)
            {
                respuesta[0] = false;
                respuesta[1] = "Fecha inicio no puede ser mayor que fecha fin";
            }

            return respuesta;
        }


        // GET: Horarios/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Horario horario = db.Horarios.Find(id);
            if (horario == null)
            {
                return HttpNotFound();
            }
            ViewBag.MedicoId = new SelectList(db.Medicos, "Id", "UsuarioId", horario.MedicoId);
            return View(horario);
        }

        // POST: Horarios/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,MedicoId,DuracionTurno,HoraInicio,HoraFin,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday,EstaBorrado,FechaCreacion")] Horario horario)
        {
            if (ModelState.IsValid)
            {
                db.Entry(horario).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.MedicoId = new SelectList(db.Medicos, "Id", "UsuarioId", horario.MedicoId);
            return View(horario);
        }

        // GET: Horarios/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Horario horario = db.Horarios.Find(id);
            if (horario == null)
            {
                return HttpNotFound();
            }
            return View(horario);
        }

        // POST: Horarios/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Horario horario = db.Horarios.Find(id);
            db.Horarios.Remove(horario);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
