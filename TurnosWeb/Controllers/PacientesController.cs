﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TurnosWeb.Models;
using TurnosWeb.Servicios.ViewModels;

namespace TurnosWeb.Controllers
{
    public class PacientesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
            
        // GET: Pacientes
        [Authorize(Roles = "Admin")]
        public ActionResult Index()
        {
            
            return View(db.Pacientes.ToList());
        }

        // GET: Pacientes/Details/5
        [Authorize(Roles = "Medico, Admin")]
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Paciente pacienteVM = db.Pacientes.Find(id);
            if (pacienteVM == null)
            {
                return HttpNotFound();
            }
            return View(pacienteVM);
        }

        // GET: Pacientes/Create
        [Authorize(Roles = "Paciente")]
        public ActionResult Create()
        {
            var CPas = CurrentPaciente();
            if (CPas != null) return RedirectToAction("Edit");

            return View();
        }

        // POST: Pacientes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles ="Paciente")]
        public ActionResult Create(Paciente pacienteVM)

        {
            if (ModelState.IsValid)
            {
                pacienteVM.Id = Guid.NewGuid();
                pacienteVM.UsuarioId = User.Identity.GetUserId();
                pacienteVM.Email = User.Identity.GetUserName();
                pacienteVM.EstaBorrado = false;
                db.Pacientes.Add(pacienteVM);
                db.SaveChanges();
                if(User.IsInRole("Paciente"))
                {
                    return RedirectToAction("Index","Home");
                }
                return RedirectToAction("Index");
            }

            return View(pacienteVM);
        }

        // GET: Pacientes/Edit/5
        [Authorize(Roles = "Paciente, Admin")]
        public ActionResult Edit(Guid? id)
        {

            var pacienteVM = CurrentPaciente();

            if (pacienteVM == null)
            {
                return HttpNotFound();
            }
            return View(pacienteVM);
        }

        // POST: Pacientes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Paciente, Admin")]
        public ActionResult Edit(Paciente pacienteVM)

        {
            if (ModelState.IsValid)
            {
                db.Entry(pacienteVM).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(pacienteVM);
        }

        // GET: Pacientes/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Paciente pacienteVM = db.Pacientes.Find(id);
            if (pacienteVM == null)
            {
                return HttpNotFound();
            }
            return View(pacienteVM);
        }

        // POST: Pacientes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult DeleteConfirmed(Guid id)
        {

            Paciente pacienteVM = db.Pacientes.Find(id);
            db.Pacientes.Remove(pacienteVM);

            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private Paciente CurrentPaciente()
        {
            var currentUserId = User.Identity.GetUserId();
            var pas = (from x in db.Pacientes
                       where x.UsuarioId == currentUserId
                       select x).FirstOrDefault();
            return pas;
        }
    }
}
