﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using TurnosWeb.Models;

namespace TurnosWeb.Controllers
{

    public class MedicosController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Medicos
        [Authorize(Roles = "Admin")]
        public ActionResult Index()
        {
            ViewBag.medico = CurrentMedico();
            var medicos = db.Medicos.Include(m => m.Especialidad);
            return View(medicos.Where(x => x.EstaBorrado == false).ToList());
        }

        // GET: Medicos/Details/5

        [Authorize(Roles = "Admin")]
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Medico medico = db.Medicos.Find(id);
            if (medico == null)
            {
                return HttpNotFound();
            }
            return View(medico);
        }

        // GET: Medicos/Create
        [Authorize(Roles = "Medico")]
        public ActionResult Create()
        {
            var CMedico =CurrentMedico();
            if (CMedico != null) return RedirectToAction("Edit");

            ViewBag.EspecialidadId = new SelectList(db.Especialidades, "Id", "Descripcion");
            return View();
        }

        // POST: Medicos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Medico")]
        public ActionResult Create([Bind(Include = "Id,EspecialidadId,Nombre,Apellido,Direccion,Telefono,Matricula")] Medico medico)
        {
            if (ModelState.IsValid)
            {
                medico.Id = Guid.NewGuid();
                medico.UsuarioId = User.Identity.GetUserId();
                medico.Email = User.Identity.GetUserName();
                db.Medicos.Add(medico);
                db.SaveChanges();                
                return RedirectToAction("Index","Home");
            }

            ViewBag.EspecialidadId = new SelectList(db.Especialidades, "Id", "Descripcion", medico.EspecialidadId);
            return View(medico);
        }

        // GET: Medicos/Edit/5

        [Authorize(Roles = "Medico, Admin")]
        public ActionResult Edit(Guid? id)
        {
            var CMedico = CurrentMedico().Id;
            Medico medico = db.Medicos.Find(CMedico);
            if (medico == null)
            {
                return HttpNotFound();
            }

            ViewBag.EspecialidadId = new SelectList(db.Especialidades, "Id", "Descripcion", medico.EspecialidadId);
            return View(medico);
        }

        // POST: Medicos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Medico, Admin")]
        public ActionResult Edit([Bind(Include = "Id,EspecialidadId,Nombre,Apellido,Direccion,Telefono,Matricula")] Medico medico)
        {
            if (ModelState.IsValid)
            {
                db.Entry(medico).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.EspecialidadId = new SelectList(db.Especialidades, "Id", "Descripcion", medico.EspecialidadId);
            return View(medico);
        }

        // GET: Medicos/Delete/5
        [Authorize(Roles = "Medico, Admin")]
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Medico medico = db.Medicos.Find(id);
            if (medico == null)
            {
                return HttpNotFound();
            }
            return View(medico);
        }

        // POST: Medicos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Medico, Admin")]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Medico medico = db.Medicos.Find(id);
            medico.EstaBorrado = true;
            db.Entry(medico).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index","Home");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private Medico CurrentMedico()
        {
            var currentUserId = User.Identity.GetUserId();
            var med = (from x in db.Medicos
                       where x.UsuarioId == currentUserId
                       select x).FirstOrDefault();
            return med;
        }
    }
}
