﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TurnosWeb.Models;
using TurnosWeb.Models.Enum;
using Microsoft.AspNet.Identity;
using TurnosWeb.Models.ViewModels;
using Newtonsoft.Json;

namespace TurnosWeb.Controllers
{
    public class TurnosController : Controller
    {
        ApplicationDbContext db = new ApplicationDbContext();
        TurnosWeb.Servicios.TurnoServicio turnoServicio = new Servicios.TurnoServicio();
        // GET: Turnos
        [Authorize(Roles = "Paciente")]
        public ActionResult Index()
        { 
            ViewBag.Medicos = db.Medicos.ToList();

            return View();
        }
         


        private bool VarificarSiExisteTurno(TurnoViewModel turno)
        {
            var idMedico = db.Medicos.Where(w => w.Email == turno.Email).FirstOrDefault().Id;
            var horario = new ApplicationDbContext();
            //var horarioDisponible = horario.Horarios.Where(x => x.Dia == (DiaDeLaSemana)turno.Fecha.DayOfWeek && x.MedicoId == idMedico && x.HoraFin <= turno.HoraTurno && x.HoraInicio >= turno.HoraTurno).ToList();
            //if (horarioDisponible.Count() > 0)
            //{
            //    var turnoDisponible = horario.Turnos.Where(x => x.MedicoId == idMedico && x.Fecha == turno.Fecha && x.HoraTurno == turno.HoraTurno).ToList();
            //    if (turnoDisponible.Count() > 0)
            //    {
            //        return false;
            //    }
            //}
            return true;
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult TurnosDisponibles(Turno turno,string Medicos)
        {
            if (db.Horarios.Any(x=>x.MedicoId==turno.MedicoId))
            { 
                return View(turnoServicio.ObtenerTurnos(turno.MedicoId, turno.Fecha).ToList());

            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
        public ActionResult Reservar(TurnoVM turno)
        {
            var usuarioID= User.Identity.GetUserId();
            var id = db.Pacientes.Where(x => x.UsuarioId == usuarioID).FirstOrDefault().Id;
            turno.PacienteId = id;
            turnoServicio.AgregarTurno(turno);
            return RedirectToAction("Index", "Home");
        }
        public ActionResult ReservaTurno()
        {
            var esp = db.Especialidades.ToList();
            return View(esp);
        }
        [HttpPost]
        public ActionResult GetProfecional(string json)
        { 

            SelectViewModel  selecthtml = JsonConvert.DeserializeObject<SelectViewModel>(json);
            var profecionales = (from p in db.Medicos.Where(w => w.Especialidad.Descripcion == selecthtml.Descripcion) select new { id = p.Id, descripcion = p.Apellido }).ToList();
            return Json(profecionales, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult Getturnos(Turno turnos, string Medicos)
        {

            Servicios.TurnoServicio ts = new Servicios.TurnoServicio();
            //var turnosts.ObtenerTurnos(turnos.MedicoId, turnos.Fecha).ToList();

            //ts.ObtenerTurnos(medico, DateTime.Now);
            return Json(new
            {
                TurnoId = new Guid(),
                NumeroTurno = 1,
                EstadoTurno = EstadoTurno.Disponible.ToString(),
                HoraTurno = new TimeSpan(12, 0, 0).Hours.ToString()
            }, JsonRequestBehavior.AllowGet);
        }
    }
}