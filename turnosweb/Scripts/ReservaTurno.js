﻿$('#especialidad').on('change', function () {
    
    var data = $.param({
        json: JSON.stringify({
            id: this.id,
            descripcion : this.value
        })
    });

    //$('#profecional').find('option').remove() ;
    $('#profecional')
    .empty()
    .append('<option selected="selected" value="0">-- Seleccione --</option>')
    ;

    $.ajax({
        url: "/Turnos/GetProfecional",
        data: data,
        method: 'POST',
        success: function (result) {
            for (var i = 0; i < result.length; i++)
                $('#profecional').append($('<option>', {
                    value: result[i].id,
                    text: result[i].descripcion
                }));
        }
    });


}); 
 
function ConsultarTurno()
{ 
    $.ajax({
        url: "/Turnos/Getturnos?medico=" + $("#profecional").val(),
        method: 'POST',
        success: function (result) {

            

            $("#turnos tbody").append("<tr><th>" + result.NumeroTurno + "</th><th>" + result.HoraTurno + "</th><th>" + result.EstadoTurno + "</th><th><input type='radio' value='1'></th></tr>");

        }
    });
}