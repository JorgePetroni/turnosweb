namespace TurnosWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MyUpdate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Horario", "DuracionTurno", c => c.Time(nullable: false, precision: 7));
            AddColumn("dbo.Horario", "FechaCreacion", c => c.DateTime(nullable: false));
            AddColumn("dbo.Horario", "DiaDeLaSemana", c => c.Int(nullable: false));
            AddColumn("dbo.Turno", "NumeroTurno", c => c.Int(nullable: false));
            DropColumn("dbo.Horario", "Monday");
            DropColumn("dbo.Horario", "Tuesday");
            DropColumn("dbo.Horario", "Wednesday");
            DropColumn("dbo.Horario", "Thursday");
            DropColumn("dbo.Horario", "Friday");
            DropColumn("dbo.Horario", "Saturday");
            DropColumn("dbo.Horario", "Sunday");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Horario", "Sunday", c => c.Boolean(nullable: false));
            AddColumn("dbo.Horario", "Saturday", c => c.Boolean(nullable: false));
            AddColumn("dbo.Horario", "Friday", c => c.Boolean(nullable: false));
            AddColumn("dbo.Horario", "Thursday", c => c.Boolean(nullable: false));
            AddColumn("dbo.Horario", "Wednesday", c => c.Boolean(nullable: false));
            AddColumn("dbo.Horario", "Tuesday", c => c.Boolean(nullable: false));
            AddColumn("dbo.Horario", "Monday", c => c.Boolean(nullable: false));
            DropColumn("dbo.Turno", "NumeroTurno");
            DropColumn("dbo.Horario", "DiaDeLaSemana");
            DropColumn("dbo.Horario", "FechaCreacion");
            DropColumn("dbo.Horario", "DuracionTurno");
        }
    }
}
